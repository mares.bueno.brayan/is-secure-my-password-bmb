const myPassword = (pass) => {
        var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
        if (pass.match(passw)) {
            return console.log("Your password is secure");
        }
        else {
            return console.log("Your password is not secure");;
        }
}

module.exports = {
    myPassword
};
