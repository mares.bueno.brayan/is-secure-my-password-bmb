# Secure Password
Dependencia utilizada para la materia de GPS verifica si tu contrasena es segura.

Uso
```
const MP = require('../src/index.js');

MP.myPassword("aqui va ala contrasena")
```

Retorna un mensaje.